//
//  NetworkManagerTest.swift
//  Revolut
//
//  Created by Manjinder Singh on 16/02/2019.
//  Copyright © 2019 Manjinder Singh. All rights reserved.
//

import XCTest
@testable import Revolut

class WebserviceHandlerTest: XCTestCase {

    var webserviceHandler: WerbserviceHandler!
    let session = MockURLSession()
    
    
    override func setUp() {
        // Put setup code here. This method is called before the invocation of each test method in the class.
        webserviceHandler = WerbserviceHandler(session: session)
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }
    
    func testResumeCalled(){
        let dataTask  = MockURLSessionDataTask()
        session.nextDataTask = dataTask
        
        guard let url = URL(string: "https://myurl") else{
            fatalError("Empty URL")
        }
        webserviceHandler.getRemoteData(url: url) { (data, error) in
            
        }
        
        XCTAssert(dataTask.resumeWasCalled)
    }

    func testGetRequestWithURL() {
        
        guard let url = URL(string: "https://myurl") else{
            fatalError("Empty URL")
        }
        
        webserviceHandler.getRemoteData(url: url) { (data, error) in
        }
        
        XCTAssert(session.lastURL == url)
    }

    func testDataIsReturned(){
    
        let expectedData = "{}".data(using: .utf8)
        session.nextData = expectedData
        
        var actualData: Data?
        webserviceHandler.getRemoteData(url: URL(string: "https://myurl")!) { (data, error) in
            actualData = data
        }
        XCTAssertNotNil(actualData)
    }
    
    func testPerformanceExample() {
        // This is an example of a performance test case.
        self.measure {
            // Put the code you want to measure the time of here.
        }
    }


}
