//
//  NetworkManagerTest.swift
//  Revolut
//
//  Created by Manjinder Singh on 16/02/2019.
//  Copyright © 2019 Manjinder Singh. All rights reserved.
//

import XCTest
@testable import Revolut

class WebserviceHandlerTest: XCTestCase {

    override func setUp() {
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    func testExample() {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct results.
    }

    func testPerformanceExample() {
        // This is an example of a performance test case.
        self.measure {
            // Put the code you want to measure the time of here.
        }
    }
    
    func testSuccessfulResponse() {
        // Setup our objects
        let session = URLSessionMock()
        let manager = NetworkManager(session: session)
        // Create data and tell the session to always return it
        let data = Data(bytes: [0, 1, 0, 1])
        session.data = data
        // Create a URL (using the file path API to avoid optionals)
        let url = URL(fileURLWithPath: "url")
        // Perform the request and verify the result
        var result: NetworkResult?
        manager.loadData(from: url) { result = $0 }
        XCTAssertEqual(result, .success(data))
    }

}
