//
//  CurrencyCell.swift
//  Revolut
//
//  Created by Manjinder Singh on 12/02/2019.
//  Copyright © 2019 Manjinder Singh. All rights reserved.
//

import UIKit
import FlagKit

protocol CurrencyDelegate: class {
    
    func selectedCurrency(cell: CurrencyCell)
}
class CurrencyCell: UITableViewCell, UITextFieldDelegate{
    
    weak var delegate: CurrencyDelegate?
    
    @IBOutlet weak var currencyIconImageView: UIImageView!
    @IBOutlet weak var currencyLabel: UILabel!
    @IBOutlet weak var fullCurrencyLabel: UILabel!
    @IBOutlet weak var amountTextField: UITextField!
    let appDelegate = UIApplication.shared.delegate as! AppDelegate
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        _ = Bundle.main.loadNibNamed("CurrencyCell", owner: self, options: nil)?[0]
        self.amountTextField.textAlignment = .right
        self.amountTextField.keyboardType = .numberPad
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    func setupCell(with currency: String, and value: String, also indexPath: IndexPath) {
        
        self.currencyIconImageView.setRounded()
        self.currencyLabel.text = currency
        
        
        let attributedString = NSMutableAttributedString()
        attributedString.append(NSAttributedString(string: value,
                                                   attributes: [.underlineStyle: NSUnderlineStyle.single.rawValue, .underlineColor:  UIColor.lightGray]))
        
        
        self.amountTextField.attributedText = attributedString
        
        let bundle = FlagKit.assetBundle
        let originalImage = UIImage(named: String(currency.prefix(2)), in: bundle, compatibleWith: nil)
        self.currencyIconImageView.image = originalImage
        self.currencyIconImageView.backgroundColor = .red
        self.fullCurrencyLabel.text = Locale.current.localizedString(forRegionCode: String(currency.prefix(2))) ?? "Other"
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        let newString = (textField.text! as NSString).replacingCharacters(in: range, with: string)
        if let cost = Double(newString) {
            
            appDelegate.selectedItem.value = cost
            print("\(appDelegate.selectedItem.value)")
        } else {
            appDelegate.selectedItem.value = 0
        }
        
        return true
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        return false
    }
}

extension UIImageView {
    
    func setRounded() {
        self.layer.cornerRadius = (self.frame.width / 2)
        self.layer.masksToBounds = true
    }
}
