//
//  ViewController.swift
//  Revolut
//
//  Created by Manjinder Singh on 11/02/2019.
//  Copyright © 2019 Manjinder Singh. All rights reserved.
//

import UIKit

let CellIdentifier = "CurrencyCell"

class ViewController: UIViewController{
    
    var dataArray: Array<(key: String, value: Double)>?
    var dataHandler: DataHandler?
    let appDelegate = UIApplication.shared.delegate as! AppDelegate
    
    var needsUpdate = true
    
    @IBOutlet weak var myTableView: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        myTableView.register(UINib(nibName: CellIdentifier, bundle: nil), forCellReuseIdentifier: CellIdentifier)
        
        myTableView.delegate = self
        myTableView.dataSource = self
        showSpinner(onView: self.view)
        myTableView.separatorStyle = .none
        
        
        weak var wSelf = self
        dataHandler = DataHandler{ (data) in
            if wSelf != nil && data != nil{
                wSelf!.dataArray = Array(data!.rates)
                
                var indexPaths = [IndexPath]()
                for x in 1...wSelf!.dataArray!.count - 1{
                    indexPaths.append(IndexPath(row: x, section: 0))
                }
                wSelf!.dataArray!.insert((key: wSelf!.appDelegate.selectedItem.currency, value: wSelf!.appDelegate.selectedItem.value), at: 0)
                
                DispatchQueue.main.async {
                    wSelf!.removeSpinner()
                    if wSelf!.needsUpdate{
                        wSelf!.myTableView.reloadData()
                        wSelf!.needsUpdate = false
                    }else{
                        self.myTableView.beginUpdates()
                        wSelf!.myTableView.reloadRows(at: indexPaths, with: UITableView.RowAnimation.none)
                        self.myTableView.endUpdates()
                    }
                }
            }
        }
    }
}

extension ViewController: UITableViewDataSource, UITableViewDelegate, CurrencyDelegate, UIScrollViewDelegate{
    
    func selectedCurrency(cell: CurrencyCell) {
        
        let indexPath = self.myTableView.indexPath(for: cell)!
        if indexPath.row > 0 {
            
            appDelegate.selectedItem.index = indexPath.row
            appDelegate.selectedItem.currency = dataArray![indexPath.row].key
            if let amountString = cell.amountTextField.text {
                appDelegate.selectedItem.value = Double(amountString) ?? 0.0
            }
            
            self.myTableView.beginUpdates()
            self.myTableView.moveRow(at: indexPath, to: IndexPath(row: 0, section: 0))
            self.myTableView.endUpdates()
            if let firstCell =  self.myTableView.cellForRow(at: IndexPath(row: 0, section: 0)) as? CurrencyCell{
                firstCell.amountTextField.becomeFirstResponder()
            }
        }
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return  1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return  self.dataArray?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell: CurrencyCell = tableView.dequeueReusableCell(withIdentifier: CellIdentifier, for: indexPath) as! CurrencyCell
        cell.delegate = self
        cell.selectionStyle = .none
        let currency = dataArray![indexPath.row].key
        
        var value =  String(format:"%.02f", dataArray![indexPath.row].value * appDelegate.selectedItem.value)
        if (indexPath.row == 0){
            value =  String(format:"%.02f",appDelegate.selectedItem.value)
        }
        cell.setupCell(with: currency, and: value, also: indexPath)
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        self.selectedCurrency(cell: tableView.cellForRow(at: indexPath) as! CurrencyCell)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 80
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        self.view.endEditing(true)
    }
}

var mySpinner : UIView?
extension UIViewController {
    func showSpinner(onView : UIView) {
        let spinnerView = UIView.init(frame: onView.bounds)
        spinnerView.backgroundColor = UIColor.init(red: 0.5, green: 0.5, blue: 0.5, alpha: 0.5)
        let ai = UIActivityIndicatorView.init(style: .whiteLarge)
        ai.startAnimating()
        ai.center = spinnerView.center
        
        DispatchQueue.main.async {
            spinnerView.addSubview(ai)
            onView.addSubview(spinnerView)
        }
        mySpinner = spinnerView
    }
    
    func removeSpinner() {
        DispatchQueue.main.async {
            mySpinner?.removeFromSuperview()
            mySpinner = nil
        }
    }
}
