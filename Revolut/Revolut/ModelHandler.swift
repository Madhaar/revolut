//
//  ModelHandler.swift
//  Revolut
//
//  Created by Manjinder Singh on 11/02/2019.
//  Copyright © 2019 Manjinder Singh. All rights reserved.
//

import Foundation

struct BaseData: Decodable {
    
    let base: String
    let date : String
    let rates: [String: Double]
    
    private enum CodingKeys: String, CodingKey{
      case  base, date, rates
    }
    
    init(from decoder: Decoder) throws {
       let container =  try decoder.container(keyedBy: CodingKeys.self)
        base = try container.decode(String.self, forKey: .base)
        date = try container.decode(String.self, forKey: .date)
        rates = try container.decode([String: Double].self, forKey: .rates)
    }
}

protocol ModelHandler {
    
    func createModelObjects(data: Data, completionHandler: @escaping (BaseData?) -> ())
}

extension ModelHandler{
    
    func createModelObjects(data: Data, completionHandler: @escaping (BaseData?) -> ()){
        
        guard let connections =  try? JSONDecoder().decode(BaseData.self, from: data) else {completionHandler(nil); return}
       // print(connections)
        completionHandler(connections)
    }
}
