//
//  DataHandler.swift
//  Revolut
//
//  Created by Manjinder Singh on 14/02/2019.
//  Copyright © 2019 Manjinder Singh. All rights reserved.
//

import Foundation
import UIKit

let BaseURL = "https://revolut.duckdns.org/latest?base="

class DataHandler: NSObject, ModelHandler {
    
    var webserviceHandler: WerbserviceHandler?
    
    typealias completionBlock = (BaseData?) -> Void
    let appDelegate = UIApplication.shared.delegate as! AppDelegate
    
    var completionBlock: completionBlock
    
    var timer: Timer?
    
    init(completionBlock: @escaping completionBlock) {
        self.completionBlock = completionBlock
        super.init()
        
        webserviceHandler = WerbserviceHandler(session: URLSession.shared)
        
        NotificationCenter.default.addObserver(self, selector: #selector(enterBackgrond), name:UIApplication.didEnterBackgroundNotification, object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(enterForeground), name:UIApplication.willEnterForegroundNotification, object: nil)
        
        timerStart()
        
    }
    
    @objc func enterBackgrond(){
        timer?.invalidate()
    }
    
    @objc func enterForeground(){
        timerStart()
    }
    
    func timerStart(){
          timer =   Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(loadData), userInfo: nil, repeats: true)
    }
    
    @objc func loadData(){
    
        webserviceHandler?.getRemoteData(url: URL(string: "\(BaseURL)\(appDelegate.selectedItem.currency)")!, callback: {[weak self] (data, error) in
            if (data != nil){
                self?.createModelObjects(data: data!, completionHandler: { (dataObj) in
                    if dataObj != nil{
                        self?.completionBlock(dataObj!)
                    }else{
                        // error
                        self?.completionBlock(nil)
                    }
                })
            }else{
                  self?.completionBlock(nil)
            }
        })
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
}
